$(document).ready(function(){
			console.log("!Estamos listos¡");

// Selectores de ID

	$('#rojo').css("background", "red")
						.css("color", "white");
	$('#amarillo').css("background", "yellow")
						.css("color", "red");
	$('#verde').css("background", "green")
						.css("color", "red");

// Selectores de clase

	var mi_clase = $('.zebra').css("padding", "5px");

	

	$('.sin_borde').click(function(){
		console.log("Click dado..")
		$(this).addClass("zebra");
	});



// Selectores por etiqueta

	var parrafos = $('p').css("cursor","pointer"); 
	// esto pone el cursor de dedo al pasar por encima

	parrafos.click(function(){
		var that = $(this);

		if(!that.hasClass('grande'))
		{
		that.addClass("grande");
		}
		else
		{
			that.removeClass("grande");
		}
	});

// Selectores por atributos

$('[title="Google"]').css('background', '#ccc');
$('[title="As"]').css('background', 'red');
$('[title="Marca"]').css('background', 'pink');
$('[title="Facebook"]').css('background', 'blue');
});

//Otros
// $('p, a').addClass('margen_superior');

var busqueda = $("#elemento2").parent().parent().find('.resaltado');

console.log(busqueda);