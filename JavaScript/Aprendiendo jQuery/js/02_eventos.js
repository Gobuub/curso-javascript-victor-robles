$(document).ready(function()
{
	// Mouse over y mouseout

	var caja = $("#caja");
/*
	caja.mouseover(function(){
		$(this).css("background","red");
	});

	caja.mouseout(function(){
		$(this).css("background","green");
	});

	// Hover hacemos lo mismo de antes pero mas rápido y sencillo, con menos código
*/
function cambiaRojo(){
		$(this).css("background","red");
	}
function cambiaVerde(){
		$(this).css("background","green");
	}
	caja.hover(cambiaRojo, cambiaVerde)

	// click y dobleclick

	caja.click(function()
	{
		$(this).css("background", "blue");
		$(this).css("color", "white");

	});

	caja.dblclick(function()
	{
		$(this).css("background", "pink");
		$(this).css("color", "yellow");

	});

// Focus y blur
var nombre =$("#nombre");
var datos = $("#datos");
nombre.focus(function(){
	$(this).css("border", "2px solid green");
});

nombre.blur(function(){
	$(this).css("border", "1px solid gray");
	
	datos.text($(this).val()).show();
});


// Mouse down y Mouse Up

	datos.mousedown(function()
	{
		$(this).css("border-color", "gray");
	});

	datos.mouseup(function()
	{
		$(this).css("border-color", "black");
	});

// Mousemove

	$(document).mousemove(function()
	{
		$("body").css("cursor", "none"); // oculta el mouse
		var sigueme = $("#sigueme");
		console.log("En X: " + event.clientX);
		console.log("En Y: " + event.clientY);
		sigueme.css("left", event.clientX).css("top", event.clientY);
	});
});