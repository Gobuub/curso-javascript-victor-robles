$(document).ready(function()
{
	reloadLinks();
	$('#add_button')
	.removeAttr('disabled')
	.click(function(){
		$('#menu').append('<li><a href ="' + $("#add_link").val() +'"/></li>'); // con esto recogemos el valor introducido en el input
		$("#add_link").val('');
		reloadLinks();
	});

	

});

function reloadLinks()
{
	$('a').each(function(index){
		var that =$(this);
		var enlace =$(this).attr('href');
		that.attr('target', 'blank'); // añade un atributo
		that.text(enlace);
	});

}