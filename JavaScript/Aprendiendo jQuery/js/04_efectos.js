$(document).ready(function(){
	var caja = $("#caja");

	caja.hide();
	$("#mostrar").show();
	$("#ocultar").hide();

	$("#mostrar").click(function(){
		$(this).hide();
		$("#ocultar").show();
		caja.slideDown('slow'); //Despliega el div
		//$("#caja").fadeIn('slow'); la difumina
		//$("#caja").show('fast'); muestra la caja de otra forma
		// efectos utiles para deplegar menús laterales.
	});

	$("#ocultar").click(function(){
		$(this).hide();
		$("#mostrar").show();
		caja.slideUp('slow'); // recoge el div a modo de persiana
		//$("#caja").fadeOut('slow');  esto lo difumina
		//$("#caja").hide('fast'); esto oculta de otra forma
	});

	$('#todo_en_uno').click(function(){
		caja.slideToggle('slow');
	});

	$('#animar').click(function(){
		caja.animate({
					  marginLeft:'500px',
					  fontSize: '40px',
					  height: '110px',
					  textAling: 'center'
					  }, 'slow')
			.animate({
				borderRadius: '900px',
				marginTop: '80px'
					}, 'slow')
			.animate({
				borderRadius: '0px',
				marginLeft: '0px'
					}, 'slow')
			.animate({
				borderRadius: '100px',
				marginTop: '00px'
					}, 'slow')
			.animate({
					  marginLeft:'500px',
					  fontSize: '40px',
					  height: '110px',
					  textAling: 'center'
					  }, 'slow');
	});


});