$(document).ready(function(){

	// Load

	//$('#datos').load("https://reqres.in/"); 

	// Get, con este método obtenemos datos desde un servidor remoto

	$.get('https://reqres.in/api/users', {page: 2}, function(response){
		response.data.forEach((element, index) =>{
			$('#datos').append("<p>" + element.first_name + " " + element.last_name+ "</p>");
		});
	});
	
	// Post este metodo sirve para enviar datos a un servidor remoto back-end
	
	$('#form').submit(function(e){
			e.preventDefault(); // esto evita la accion por defecto del formulario
			var usuario = {
			name: $('input[name="name"]').val(),
			web: $('input[name="web"]').val(),
		};
		
	/*	$.post($(this).attr("action"), usuario, function(response){
			console.log(response);
		}).done(function(){
			alert("!Usuario registrado correctamente¡")
		});

	*/
		$.ajax({
			type: 'POST',
			url: $(this).attr("action"),
			data: usuario,
			beforeSend: function(){
				console.log("Enviando usuario...");
			},
			success: function(response){
				console.log(response);
			},
			error: function(){
				console.log("A ocurrido un error");
			},
			timeout: 1000
		});

		return false; // con esto evitamos que abra una pagina nueva
	});
});