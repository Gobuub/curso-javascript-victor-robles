$(document).ready(function(){
	// Mover elemento por la pagina
	$('.elemento').draggable();

	// Cambiar el tamaño
	$('.elemento').resizable();

	//Seleccionar elementos
	//$('.lista_seleccionable').selectable();

	//Ordenar elementos
	$('.lista_seleccionable').sortable({
		update: function(event, ui){
			console.log("La lista ha cambiado");
		}
	});

	// drop
	$('#elemento_movido').draggable();
	$('#area').droppable({
		drop: function(){
			console.log("Has soltado algo dentro del área");
		}
	});

	// efectos

	$("#mostrar").click(function(){
	//	$(".caja_efectos").toggle("explode"); // con este método es como si explotara
	//	$(".caja_efectos").toggle("blind");  // aparece con efecto persiana
	//	$(".caja_efectos").toggle("fade");  // aparece y desaparece con efecto difuminado
	//	$(".caja_efectos").toggle("slide"); // aparece y desaparece con efecto cortina
	//	$(".caja_efectos").toggle("puff");
	//	$(".caja_efectos").toggle("scale");
		$(".caja_efectos").toggle("shake", "slow"); // vibra al aparecer o antes de desaparecer
	});

	// Tooltip
	$(document).tooltip();

	// Dialog
	$("#lanzar_popup").click(function(){
		$("#popup").dialog();
	});

	// Datepicker

	$("#calendario").datepicker();


	// Tabs
	$("#tabs").tabs();



});