import { Injectable} from '@angular/core';
import { Zapatilla } from '../models/zapatilla';

@Injectable()
export class ZapatillaService{
	public zapatillas: Array<Zapatilla>;
	constructor(){
		this.zapatillas = [
			new Zapatilla('Nike Air Max', 'Nike', 'Violeta', 100, true),
			new Zapatilla('SolarBoost', 'Adidas', 'Rojo', 160, true),
			new Zapatilla('UltraBoost', 'Adidas', 'Negro', 100, true),
			new Zapatilla('Super Star', 'Adidas', 'Blanco', 120, false),
			new Zapatilla('One', 'Hoka', 'Amarillo', 200, true),
			new Zapatilla('NB Run Gel', 'New Balance', 'Violeta', 150, true)


		];
	}

	getTexto(){
		return "Hola Mundo desde un servicio";
	}

	getZapatillas(): Array<Zapatilla>{
		return this.zapatillas;
	}
}