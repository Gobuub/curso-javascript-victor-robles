import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';

@Component({
	selector: 'videojuego',
	templateUrl: './videojuego.component.html'
	

})// IMPORTANTE los componentes no se cierran con ';'

export class VideojuegoComponent implements OnInit, DoCheck, OnDestroy{
	public titulo: string;
	public listado: string;
	

	constructor (){
		this.titulo = "Componente de Videojuegos";
		this.listado = "Listado de los Videojuegos más populares";


		//console.log("Se ha cargado el componente: Videojuego");
	}

	// Este evento se ejecuta cuando se inica la pagina
	ngOnInit(): void{
		//console.log("OnInit ejecutado");
	}

	// Este se ejecuta siempre que sucede algún cambio en el componente

	ngDoCheck(): void{
		//console.log("DoCheck ejecutado");
	}

	//Solo se ejecuta cuando eliminemos el componente

	ngOnDestroy(): void{
		//console.log("OnDestroy ejecutado");
	}

	cambiarTitulo(){
		this.titulo = "Nuevo Titulo del Componente";
	}
}