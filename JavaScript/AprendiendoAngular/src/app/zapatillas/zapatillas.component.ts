import { Component, OnInit } from '@angular/core';
import { Zapatilla } from '../models/zapatilla';
import { ZapatillaService} from '../service/zapatilla.service';

@Component({
	selector: 'zapatillas',
	templateUrl: './zapatillas.component.html',
	providers: [ZapatillaService]

})

export class ZapatillasComponent implements OnInit {
	public titulo: string = "Calzado Deportivo"; // indicamos el título del elemento
	public zapatillas!: Array<Zapatilla>;
	public marcas: String[];
	public color: string;
	public mi_marca: string;

	constructor(
		private _zapatillaService: ZapatillaService

		){
		this.mi_marca = "";
		this.color = "violeta";
		this.marcas = new Array();
		/*this.zapatillas = [
			new Zapatilla('Nike Air Max', 'Nike', 'Violeta', 100, true),
			new Zapatilla('SolarBoost', 'Adidas', 'Rojo', 160, true),
			new Zapatilla('UltraBoost', 'Adidas', 'Negro', 100, true),
			new Zapatilla('Super Star', 'Adidas', 'Blanco', 120, false),
			new Zapatilla('One', 'Hoka', 'Amarillo', 200, true),
			new Zapatilla('NB Run Gel', 'New Balance', 'Violeta', 150, true)


		];*/
		

	}

	ngOnInit(){
		this.zapatillas = this._zapatillaService.getZapatillas();
		alert(this._zapatillaService.getTexto());
		this.getmarcas();
	}

	getmarcas(){
		this.zapatillas.forEach((zapatilla, index) =>{
			if(this.marcas.indexOf(zapatilla.marca) < 0){
				this.marcas.push(zapatilla.marca);
			}
			
		});

		console.log(this.marcas);
	}

	// Esto nos muestra la marca introducida en el input
	getMarca(){
		alert(this.mi_marca);
	}
	// con este método guardamos la marca introducida y la añade a nuestro array de marcas
	addMarca(){
		this.marcas.push(this.mi_marca);

	}
	// he marcado el tipo de dato que lleva "indice" a 'any' porque TS no dejaba continuar
	borrarMarca(indice: any){
		// delete this.marcas[index];
		this.marcas.splice(indice, 1);
	}

	onBlur(){
		console.log("Has salido del input");
	}

	mostrarPalabra(){
		alert(this.mi_marca);
	}
}