'use strict'

// configuración de express

var express = require('express');
var bodyParser = require('body-parser');

var app = express();


// cargar archivos de rutas

var project_routes = require('./routes/project');


// middlewares, es un metodo que se ejecuta antes de una petición

app.use(bodyParser.urlencoded({extended:false})); // necesario para iniciar body-parser
app.use(bodyParser.json()); // nos transforma todo lo que llegue en un objeto json


// CORS
//Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'/*aquí abría que poner la url en vez de * */);
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// rutas
app.use('/api', project_routes);


// exportar

module.exports = app;