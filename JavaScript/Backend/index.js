'use strict'

var mongoose = require('mongoose'); 
// esta variable la creamos para conectarnos a la base de datos

var app = require('./app'); 

//con esta variable cargamos la configuración de express

var port = 3700;

// puerto del servidor

// creamos una promesa para iniciar la conexión a la base de datos
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/portafolio')
// esta es la dirección de nuestra base de datos
		.then(()=>{
			console.log("Conectado a la base de datos correctamente ...");
	// este mensaje aparecerá cuando nos conectemos satisfactoriamente

	// creación del servidor

		app.listen(port, ()=>{
			console.log("Servidor activo en: localhost:3700");
		});
		})
		.catch(err => console.log(err));
	// si hay algún error con este método lo capturamos y nos lo escribe por consola.