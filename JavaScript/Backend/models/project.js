'use strict'

var mongoose = require('mongoose');
// con esto podemos acceder a mongo

var Schema = mongoose.Schema;

// con esta variable cargamos el esquema


// molde de nuestro archivo que se cargara en la base de datos

var ProjectSchema = Schema({
	name: String,
	description: String,
	category: String,
	year: Number,
	langs: String,
	image: String
});


// con el siguiente método exportamos nuestro modelo para que pueda ser usado en nuestra app
module.exports = mongoose.model('Project', ProjectSchema);

// si no existiera el parametro Project en nuestr base de datos mongoose nos
// crearía un nuevo elemento donde se cargarían los modelos que generaramos
// Project != => mongoose crea 'projects' y ahí guardaría las colecciones nuevas
// si ya existe los guarda ahí directamente