'use strict'

var express = require('express');

var ProjectController = require('../controllers/project');

var router = express.Router();

//var multipart = require('connect-multiparty');
//var multipartMiddleware = multipart({uploadDir: './uploads'});


//const cors = require('cors');
//router.use(cors());


// ------------------- Multer ---------------------------------

var crypto = require('crypto')

var multer = require('multer');

const storage = multer.diskStorage({

  destination(req, file, cb) {

    cb(null, './uploads/albums');

  },

  filename(req, file = {}, cb) {
    const { originalname } = file;
    const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
    // cb(null, `${file.fieldname}__${Date.now()}${fileExtension}`);
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + fileExtension);
    });
  },

});

var mul_upload = multer({dest: './uploads/albums',storage});

router.get('/home', ProjectController.home);
router.post('/test', ProjectController.test);
router.post('/saveProject', ProjectController.saveProject);
router.get('/project/:id?', ProjectController.getProject);
router.get('/projects', ProjectController.getProjects);
router.put('/project/:id', ProjectController.updateProject);
router.delete('/project/:id', ProjectController.deleteProject);
//router.post('/uploadImage/:id', multipartMiddleware, ProjectController.uploadImage);

//router.post('/uploadImage/:id', ProjectController.uploadImage);
router.post('/uploadImage/:id',[mul_upload.single('image')], ProjectController.uploadImage);
router.get('/getImage/:image', ProjectController.getImageFile);
module.exports = router;