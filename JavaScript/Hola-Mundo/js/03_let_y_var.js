// pruebas con let y var
'use strcit'

// Prueba con var
var numero = 40;
console.log(numero); //valor 40

if(true){

	var numero=50;
	console.log(numero); // valor 50
}

console.log(numero); // valor con 50

//Prueba con let

var texto = "Curso JS con Victor Robles";
console.log(texto); // "valor"

if (true) {

	let texto="Curso Laravel 5 con Victor Robles";
	console.log(texto); // valor laravel 5;
}

console.log(texto); // valor js