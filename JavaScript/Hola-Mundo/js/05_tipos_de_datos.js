'use strict'

// Operadores

var numero1 = 7;
var numero2 = 12;

var operacion = numero1 % numero2;

alert("El resultado de la operacion es: " + operacion);


// Tipos de datos

var numero_entero = 44; // enteros
var cadena_texto = "hola"; // string o cadena de texto
var verdadero_o_falso = true; // booleanos

var numero_falso = "33";

console.log(Number(numero_falso) + 7);
console.log(parseInt(numero_falso) + 7);

numero_falso = "33.4";

console.log(parseFloat(numero_falso) + 7);

console.log(typeof numero_entero);
console.log(typeof cadena_texto);
console.log(typeof verdadero_o_falso);
console.log(typeof numero_falso);


// Number transforma un string en numero entero
// parseInt transforma un string en un entero
// parseFloat transforma un string en un numero decimal
// String transforma cualquier variable a una cadena de texto