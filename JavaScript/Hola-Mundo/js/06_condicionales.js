'use strict'

//Condicional if
// Si a es igual a b haz algo

var edad = 12;
var nombre = "Enrique Revuelta";

/*
Operadores relacionales
	mayor: >
	menor: <
	mayor o igual: >=
	menor o igual: <=
	igual: ==
	distinto: !=
*/

// si pasa esto
if (edad >= 18) {

// ejecuta esto
console.log(nombre + " tiene " + edad + " años, es mayor de edad");

	if (edad <= 33) {

		console.log("Todavia eres milenial");
	}

	else if(edad >= 70 ){

		console.log("Eres Anciano");
	}
	else {

		console.log("No eres milenial");
	}
}
else{

	console.log(nombre + " tiene " + edad + " años, es menor de edad");
}

/*
	and (Y): &&
	or (o): ||
	negación: !
*/


var year = 2021;
// Negación

if (year != 2018) {

	console.log("El año no es 2018, realmente es: " + year);
}

// and

if (year >= 2000 && year <= 2021)
{

	console.log("Estamos en la edad actual");
}
else{
	console.log("Estamos en la era post Coivd-19");
}

//or

if (year == 2011 || year == 2021) {

	console.log("El año acaba en 1");
}
