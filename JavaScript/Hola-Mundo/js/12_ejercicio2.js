'use strict'

/*
Utilizar un bucle, mostrar suma y la media de los numeros introducidos
hasta introducir un numero negativo y ahí mostrar el resultado
*/


var suma = 0;
var contador = 0;

do{

	var numero = parseInt(prompt("Introduce numeros hasta que metas uno negativo", 0));

	if(isNaN(numero)){

		numero = 0;
	}
	else if (numero >= 0){

		suma = suma + numero;

		// suma += numero; esto sería lo mismo

		contador++;
	}

	console.log(suma);
	console.log("Has metido " + contador + " numeros");
}
while (numero >= 0){

	alert("La suma de todos los números es: " + suma);
	alert("La media de todos los numeros es: " + (suma / contador));
}
