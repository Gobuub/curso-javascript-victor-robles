'use strict'

/* Calculadora
- Pida dos numeros
- Si metemos uno mal nos los vuelva a pedir
- Mostar en el cuerpo, en una alerta y en consola, el resultado
de sumar, multiplicar, dividir y restar.

*/

var numero1 = parseInt(prompt("Introduce el primer número", 0));
var numero2 = parseInt(prompt("Introduce el segundo numero", 0));

while(numero1<0 || numero2<0 || isNaN(numero1 || isNaN(numero2))){

	numero1 = parseInt(prompt("Introduce el primer número", 0));
	numero2 = parseInt(prompt("Introduce el segundo numero", 0));
}

var resultado = "La suma es: " + (numero1+numero2) + "<br/>" +
				"La resta es: " + (numero1-numero2) + "<br/>" +
				"La multiplicación es: " + (numero1*numero2) + "<br/>"+
				"La Divisón es: " + (numero1/numero2) + "<br/>";

var resultadoCMD = "La suma es: " + (numero1+numero2) + "\n" +
				"La resta es: " + (numero1-numero2) + "\n" +
				"La multiplicación es: " + (numero1*numero2) + "\n"+
				"La Divisón es: " + (numero1/numero2) + "\n";

alert(resultadoCMD);
console.log(resultadoCMD);
document.write(resultado);
