'use strict'

// Son arrays dentro de arrays

var categorias = ["Acción", "Comedia", "Terror"];
var peliculas = ["Los Vengadores", "8 Apellidos Vascos", "Pesadilla en Eml Street"];


peliculas.sort(); // ordena el array alfabeticamente por defecto

peliculas.reverse(); // ordena el array de atrás hacia adelante, le da la vuelta.

console.log(peliculas);

var cine = [categorias,peliculas];
/*
console.log(cine[0][1]);
console.log(cine[1][2]);

var elemento = prompt("Introduce tu pelicula");

do // hace esto hasta
{
	var elemento = prompt("Introduce tu pelicula");
	peliculas.push(elemento);
}

while (elemento != "Fin"){} // que se cumpla esta condición

	
peliculas.pop(); // Borra el ultimo elemento del array


document.write("<h1>Listado de Peliculas</h1>");
document.write("<h3><ul>")
for (var i =0; i < peliculas.length; i++) {
	document.write("<li>" + peliculas[i]+ "</li>")
}

document.write("</ul></h3>");
*/

var indice = peliculas.indexOf("8 Apellidos Vascos"); // esto elimina el elemento de un array

console.log(indice);

if(indice >-1)
{
	peliculas.splice(indice, 1);

}

var peliculas_string = peliculas.join();

var cadena ="texto1, texto2, texto3";
var cadena_array = cadena.split(", "); // esto convierte la cadena en un array, nos separa todo lo que
										// que va despues de ", ".

console.log(cadena_array);
