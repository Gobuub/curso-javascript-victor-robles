'use strict'

/*
Progama que:
1.Pida 6 numeros por pantalla y los meta en el array
2. Mostar array por consola en el body y en consola
3. Ordenar el array y mostrarlo
4. Invertir el array y mostrarlo
5. Mostrar cuantos elementos tiene el array
6. Busqueda de número introducido por el usuario, que nos diga si lo encuentra y su indice.
*/

//var numeros = new Array(6);

// Para que quedara mejor ordenado podría meterse todo en una funcion
// mostrar números.
var numeros = [];

for (var i = 0; i <= 5; i++) { // pedir 6 numeros
	//numeros[i]= parseInt(prompt("Introduce un número", 0));
	numeros.push(parseInt(prompt("Introduce un número", 0)));
}

document.write("<h1>Ejercicio de Arrays</h1>"); 
document.write("<h2>Numeros introducidos por el usuario</h2>");

numeros.forEach((numero, index) => // mostrar numeros en el body de la pagina
{
	document.write("<strong>" + numero + "</strong><br/>")
});

console.log(numeros); // Mostramos array por consola

document.write("<h2>Numeros introducidos por el usuario ordenados</h2>");

numeros.sort(function(a,b){return a-b}); // ordenar los numeros de mayor a menor con la funcion callback

numeros.forEach((numero, index) => // mostrar numeros en el body de la pagina
{
	document.write("<strong>" + numero + "</strong><br/>")
});


document.write("<h2>Numeros introducidos por el usuario ordenados al revés</h2>");

numeros.reverse(function(a,b){return a-b}); // ordenar los numeros al revés

numeros.forEach((numero, index) => // mostrar numeros en el body de la pagina
{
	document.write("<strong>" + numero + "</strong><br/>")
});

var longitud_array = numeros.length;

console.log(longitud_array);
document.write("<h2> El Array tiene " + longitud_array + " numeros</h2>");

var numero_introducido = parseInt(prompt("Introduce el numero a buscar", 0));

var busqueda = numeros.some(numero => numero == numero_introducido);

console.log(busqueda);

document.write("<h2> El numero introducido al final está en la lista?</h2><br> " + busqueda);


