'use strict'

// DOM Document Object model

function cambiaColor(color){

	caja.style.background = color;
}

//var caja = document.getElementById("micaja") ;

var caja = document.querySelector("#micaja"); // otro metodo para seleccionar un elemento
											  // en el HTML con ID concreto

caja.innerHTML = "Texto en la caja desde JS"; // A traves de esto cambiamos el mensaje de la "caja"
caja.style.background = "red"; // con esto cambiamos los atributos como si fuera CSS
caja.style.padding = "200px";
caja.style.color = "white";
caja.className = "Hola"; // con esto cambiamos los atributos de HTML


// Conseguir elementos por su etiqueta

var todosLosDivs = document.getElementsByTagName('div');

//todosLosDivs.forEach((valor, indice) => 
//{

var seccion = document.querySelector("#miseccion");
var hr = document.createElement("hr");
var valor;


for(valor in todosLosDivs){
	if (todosLosDivs[valor].textContent) 
	{
	console.log(typeof todosLosDivs[valor] == 'string');
	var parrafo = document.createElement("p");
	var texto =document.createTextNode(todosLosDivs[valor].textContent);
	parrafo.append(texto);
	seccion.append(parrafo);
	
	}	
}

seccion.append(hr);
//});






// Conseguir elementos por su clase css


var divsRojos = document.getElementsByClassName('rojo');
var divsAmarillos = document.getElementsByClassName('amarillo');

console.log(divsAmarillos);

divsAmarillos[0].style.background = "yellow";

var div;
for (div in divsRojos )
{
if (divsRojos[div].className == 'rojo')
	{	
	divsRojos[div].style.background ="red";
	}
}

//Query selector

var id = document.querySelector("#encabezado");

console.log(id); 


var claseRojo = document.querySelectorAll("div.rojo"); // seleccionar todos los elementos con esa etiqueta


console.log(claseRojo);

