'use strict'


// BOM - Browser object model


function getBom(){

	console.log(window.innerWidth); // estos nos da los valores de la ventana del navegador
	console.log(window.innerHeight);
	console.log(window.location); // nos da la info de la pagina
}

getBom();

function getBomScreen()
{
	console.log(screen.width); // estos nos da los valores de la pantalla
	console.log(screen.height);

}

getBomScreen();

function redirect(url) // esta funcion nos redirige a la pagina que pongamos en consola
{

	window.location.href = url;
}

function abrirVentana(url)
{
	window.open(url, "", "width=400,height=400"); //nos abre una ventana nueva con esas caracteristicas y
													// en la url que le indiquemos.

}

abrirVentana();
