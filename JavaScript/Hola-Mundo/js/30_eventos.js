'use strict'

// Eventos

//De raton


// evento load

window.addEventListener('load', () => {
		function cambiarColor()
	{
		console.log("Me has dado click");
		var bg = boton.style.background;
		if(bg == "green")
		{
			boton.style.background = "red";
			
		}
		else
		{
			boton.style.background = "green";
		}

		boton.style.padding = "10px";
		boton.style.border = "1px solid #ccc";

		return true;
	}

	var boton = document.querySelector("#boton");

	// click

	boton.addEventListener('click', function(){
		cambiarColor();
		this.style.border = "5px solid black"; // this te permite acceder a ese elemento que estamos modificando
	});

	// mouse over

	boton.addEventListener('mouseover', function(){ // este nos cambia el color del boton al pasar el raton por encima

		boton.style.background = "#ccc";
	});

	boton.addEventListener('mouseout', function(){ // este nos cambia el color del boton al salir con el raton de el

		boton.style.background = "white";
	});

	// Focus
	var input = document.querySelector('#campo_nombre');
	input.addEventListener('focus', function(){ 

		console.log("[focus] Estas dentro del input");
	});


	// Blur

	input.addEventListener('blur', function(){ 

		console.log("[blur] Estas fuera del input");
	});
	//Keydown

	input.addEventListener('keydown', function(){ 

		console.log("[keydown] Estas pulsando la tecla ", String.fromCharCode(event.keyCode));
	});
	//Keypress

	input.addEventListener('keypress', function(){ 

		console.log("[keypress] Tecla presionada ", String.fromCharCode(event.keyCode));
	});
	//Keyup
	input.addEventListener('keyup', function(){ 

		console.log("[keyup] Tecla soltada ", String.fromCharCode(event.keyCode));
	});

}); // end load

