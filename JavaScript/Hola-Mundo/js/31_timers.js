'use strict'


window.addEventListener('load', function(){

	function intervalo()
	{
		var tiempo = setInterval(function(){ // se ejecuta en bucle
			
			console.log('Set interval ejecutado');

			var encabezado = document.querySelector("h1");
			if(encabezado.style.fontSize == "50px")
			{

				encabezado.style.fontSize = "30px";
				encabezado.style.color = "yellow";
			}
			else
			{
				encabezado.style.fontSize = "50px";
				encabezado.style.color = "red";
			}
			}, 1000);

		return tiempo;
	}

	var tiempo = intervalo();
	
	//Timers
/*
	var tiempo = setTimeout(function(){ // Solo se ejecuta una vez

		console.log('Set interval ejecutado');

		var encabezado = document.querySelector("h1");
		if(encabezado.style.fontSize == "50px")
		{

			encabezado.style.fontSize = "30px";
			encabezado.style.color = "yellow";
		}
		else
		{
			encabezado.style.fontSize = "50px";
			encabezado.style.color = "red";
		}
	}, 500);
*/
	var stop = document.querySelector("#stop");

	stop.addEventListener("click", function(){
		alert("Has parado el evento en bucle");
		clearInterval(tiempo);
	});

	var start = document.querySelector("#start");
	start.addEventListener("click", function(){
		alert("Has iniciado el evento en bucle");
		intervalo();
	});
});