'use strict'

// Ejercicios

// Crear

window.addEventListener('load', function(){
	console.log("DOM cargado");

	var formulario = document.querySelector("#formulario");

	var box_dashed = document.querySelector(".dashed");
	box_dashed.style.display = "none";

	formulario.addEventListener('submit', function(){
		console.log("Evento Submit Cargado")

		var nombre = document.querySelector("#nombre").value;
		var apellidos = document.querySelector("#apellidos").value;
		var edad = parseInt(document.querySelector("#edad").value);

		if (nombre.trim() == null || nombre.trim().length == 0)
		{
			alert("El Nombre no es válido");
			return false;
		}

		if (apellidos.trim() == null || apellidos.trim().length == 0)
		{
			alert("Los Apellidos no son válidos");
			return false;
		}

		if (edad == null || edad <= 0 || isNaN(edad))
		{
			alert("La Edad no es válida");
			return false;
		}

		box_dashed.style.display = "block";

		var datos_usuario = [nombre, apellidos, edad]; 

		var indice;
		for (indice in datos_usuario)
		{
			var parrafo = document.createElement("p");
			parrafo.append(datos_usuario[indice]);
			box_dashed.append(parrafo);
		}	
	});
});