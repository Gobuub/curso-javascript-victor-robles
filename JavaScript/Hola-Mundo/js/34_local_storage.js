'use strict'

// Localstorage

// comprobar disponibilidad del local storage
if(typeof(Storage) !== "undefined"){
	console.log("Local Storage disponible");
}
else
{
	console.log("Local Storage no disponible");
}

// guardar datos


localStorage.setItem("Titulo", "Curso de JS de Victor Robles");

// Recuperar elemento 

document.querySelector('#peliculas').innerHTML = localStorage.getItem("Titulo");


// guardar objetos

var usuario = {
	nombre: "Enrique Revuelta",
	email: "botica@msn.com",
	web: "none@none.com"
};

localStorage.setItem("usuario", JSON.stringify(usuario)); 

// recuperar objeto
var user_js = JSON.parse(localStorage.getItem("usuario"));

console.log(user_js);
document.querySelector('#datos').append(user_js.nombre + " - "+ user_js.email+ " - " + user_js.web);

// borrar elementos

localStorage.removeItem("usuario");

// vaciar local storage

localStorage.clear();


