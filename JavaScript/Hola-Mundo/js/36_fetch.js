'use strict'

// Fetch (ajax) y peticiones a servicios / apis rest


var div_usuarios = document.querySelector("#usuarios");
var div_alumno = document.querySelector("#alumno");
var div_janet = document.querySelector("#janet");

	getUsuarios()
	.then(data => data.json())		// aqui guardamos la info en un objeto json
	.then(users => {				// con esto podemos acceder a los datos del json
		listadoUsuarios(users.data);

		return getInfo();
	})
	.then(data => {
		div_alumno.innerHTML = data;

		return getJanet();
	})
	.then(data => data.json())
	.then(user =>{
		mostrarJanet(user.data);
	})
	.catch(error =>{
		alert("Error en las peticiones");
	});
	


	function getUsuarios()
	{
		return fetch('https://reqres.in/api/users'); // peticion al servidor externo
	}

	function getJanet()
	{
		return fetch('https://reqres.in/api/users/2'); // peticion al servidor externo
	}

	function getInfo()
	{
		var alumno = {
			nombre: 'Enrique',
			apellidos: 'Revuelta García',
			email: 'hoirubikes@gmail.com'
		};
		return  new Promise((resolve, reject) =>
		{
			var alumno_string ="";
			setTimeout(function(){
				alumno_string = JSON.stringify(alumno);
				if(typeof alumno_string != 'string' || alumno_string == '') return reject('error');
				return resolve(alumno_string);
			}, 3000);

			
			
			
		})
		
	}

	function listadoUsuarios (usuarios)
	{
		usuarios.map((user, i) => 
		{ 
			let nombre = document.createElement('h3');
			nombre.innerHTML = i +'. '+ user.first_name + ' ' + user.last_name;
			div_usuarios.appendChild(nombre);
			document.querySelector(".loading ").style.display = 'none';
		});
	}

	function mostrarJanet (user)
	{
			let nombre = document.createElement('h3');
			let avatar = document.createElement('img');
			nombre.innerHTML = user.first_name + ' ' + user.last_name;
			avatar.src = user.avatar;
			avatar.width = '100';
			div_janet.appendChild(nombre);
			div_janet.appendChild(avatar);
			document.querySelector("#janet .loading ").style.display = 'none';
		
	}