$(document).ready(function(){

	// Slider
	if(window.location.href.indexOf('index') > -1)
	{
		$('.bxslider').bxSlider({
	    mode: 'fade',
	    captions: true,
	    slideWidth: 1920,
	    responsive: true,
	    pager: false
  		});
	}
	// Post
	if(window.location.href.indexOf('index') > -1)
	{
		var posts = [

			{
				title: "Bienvenido a Hoīru  Bikes",
				date: 'Publicado el ' + moment().format("LLLL"),
				content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at dictum erat, at varius odio. Etiam faucibus viverra massa, ac ultricies neque consectetur a. Suspendisse potenti. Phasellus sagittis est urna, at aliquam velit faucibus eget. Quisque ac ultrices neque. Proin in magna eget ex pulvinar tempus vel in justo. Sed at lacus enim. Nullam at erat libero. Quisque vel viverra tellus. Morbi suscipit finibus purus vel porta. Sed a odio efficitur, molestie elit vitae, sagittis mauris. Pellentesque bibendum ipsum ac odio molestie facilisis.'
			},
			{
				title: "Tenemos la bici de tus sueños y aún no lo sabes",
				date: 'Publicado el ' + moment().format("LLLL"),
				content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at dictum erat, at varius odio. Etiam faucibus viverra massa, ac ultricies neque consectetur a. Suspendisse potenti. Phasellus sagittis est urna, at aliquam velit faucibus eget. Quisque ac ultrices neque. Proin in magna eget ex pulvinar tempus vel in justo. Sed at lacus enim. Nullam at erat libero. Quisque vel viverra tellus. Morbi suscipit finibus purus vel porta. Sed a odio efficitur, molestie elit vitae, sagittis mauris. Pellentesque bibendum ipsum ac odio molestie facilisis.'
			},
			{
				title: "Dale un vistazo a nuestros cuadros de carretera",
				date: 'Publicado el ' + moment().format("LLLL"),
				content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at dictum erat, at varius odio. Etiam faucibus viverra massa, ac ultricies neque consectetur a. Suspendisse potenti. Phasellus sagittis est urna, at aliquam velit faucibus eget. Quisque ac ultrices neque. Proin in magna eget ex pulvinar tempus vel in justo. Sed at lacus enim. Nullam at erat libero. Quisque vel viverra tellus. Morbi suscipit finibus purus vel porta. Sed a odio efficitur, molestie elit vitae, sagittis mauris. Pellentesque bibendum ipsum ac odio molestie facilisis.'
			},
			{
				title: "Dale un vistazo a nuestros cuadros de MTB",
				date: 'Publicado el ' + moment().format("LLLL"),
				content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at dictum erat, at varius odio. Etiam faucibus viverra massa, ac ultricies neque consectetur a. Suspendisse potenti. Phasellus sagittis est urna, at aliquam velit faucibus eget. Quisque ac ultrices neque. Proin in magna eget ex pulvinar tempus vel in justo. Sed at lacus enim. Nullam at erat libero. Quisque vel viverra tellus. Morbi suscipit finibus purus vel porta. Sed a odio efficitur, molestie elit vitae, sagittis mauris. Pellentesque bibendum ipsum ac odio molestie facilisis.'
			},	
			{
				title: "Montajes a la carta de ruedas",
				date: 'Publicado el ' + moment().format("LLLL"),
				content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at dictum erat, at varius odio. Etiam faucibus viverra massa, ac ultricies neque consectetur a. Suspendisse potenti. Phasellus sagittis est urna, at aliquam velit faucibus eget. Quisque ac ultrices neque. Proin in magna eget ex pulvinar tempus vel in justo. Sed at lacus enim. Nullam at erat libero. Quisque vel viverra tellus. Morbi suscipit finibus purus vel porta. Sed a odio efficitur, molestie elit vitae, sagittis mauris. Pellentesque bibendum ipsum ac odio molestie facilisis.'
			},
		];
	
		posts.forEach((item, index) =>
		{
			var post = `
					<article class="post">
						<h2>${item.title}</h2>
							<span class="date">${item.date}</span>
							<p>
								${item.content}
							</p>
							<a href="#" class="button-more"> Leer más</a>
					</article>
			`;
			$("#posts").append(post);
		});
	}
	// selector de tema
	var theme = $('#theme');
	$('#to_black').click(function(){
		theme.attr("href","css/black.css");
	});
	$('#to_red').click(function(){
		theme.attr("href","css/red.css");
	});
	$('#to_blue').click(function(){
		theme.attr("href","css/blue.css");
	});

	// scroll arriba web

	$(".up").click(function(e)
	{
		e.preventDefault();

		$(`html, body`).animate({
			scrollTop: 0
		}, 500);

		return false;
	});


	// login falso

	$('#login form').submit(function(){
		var form_name = $('#form_name').val();

		localStorage.setItem('form_name', form_name);
	});


	var form_name = localStorage.getItem("form_name");

	if(form_name != null && form_name != "undefined")
	{
		var about_parrafo = $("#about p");

	about_parrafo.html("<br><strong>Bienvenido, " + form_name + "</strong><br/>");
	about_parrafo.append("<br/><a href='#' id='logout'>Cerrar Sesión</a>");

	$("#logout").click(function(){
		localStorage.clear();
		location.reload();
	});

	$('#login').hide();
	}

	if(window.location.href.indexOf('about') > -1){

		$("#acordeon").accordion();

	}

	if(window.location.href.indexOf('clock') > -1){
		setInterval(function(){
			var clock = moment().format('h:mm:ss a');
			$("#clock").html(clock);
		}, 1000);
	}

	// validación
	if(window.location.href.indexOf('contact') > -1)
	{
		$("form input[name='date']").datepicker({
			dateFormat:'dd-mm-yy'
		});
		$.validate({
			lang: "es"
			
		});
	}
});

