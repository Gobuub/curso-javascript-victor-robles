import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public email: string;

  constructor() { 
    this.title = "Enrique Revuelta García";
    this.subtitle = "Podólogo en evolución => Desarrollador Apps, Webs y Videojuegos";
    this.email = "ortocadp@gmail.com"

  }

  ngOnInit(): void {
  }

}
