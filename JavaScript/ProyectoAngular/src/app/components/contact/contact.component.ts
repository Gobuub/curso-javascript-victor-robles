import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';



@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  public widthSlider: number;
  public anchuraToSlider: any;
  public autor: any;
  @ViewChild('texto', {static: true}) texto?:any;

  constructor() 
  { 
    this.widthSlider = 400;
    this.autor = "";
    this.texto = "";
    
  }

  ngOnInit(): void 
  {
    var classic_option = document.querySelector('#contacto')!.textContent; //opcion clasica con JS
   alert("Este mensaje se ha creado con JS. " + classic_option);
    var viewChild_option =this.texto.nativeElement.textContent; // opción Angular ViewChild
   alert("Este mensaje se creo con ViewChild. " + viewChild_option);
    
  }

  cargarSlider()
  {
    this.anchuraToSlider = null;
    this.anchuraToSlider = this.widthSlider;
  }

  resetSlider()
  {
    this.anchuraToSlider = false;
  }

  catchAutor(event:any)
  {
    this.autor = event;
  }
}
