import { Component, OnInit } from '@angular/core';
import { Project } from '../../models/project';
import { ProjectService } from '../../services/project.service';
import { UploadService } from '../../services/upload.service';
import { Global} from '../../services/global';
import { Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [ProjectService, UploadService] 
})
export class CreateComponent implements OnInit 
{

  public title: string;
  public project: Project;
  public save_project: any;
  public status: string;
  public filesToUpload: Array<File>;
  public url: string;

  constructor(
    private _projectService: ProjectService,
    private _uploadService: UploadService,
    private _router: Router,
    private _route: ActivatedRoute

    ) { 
      this.title = "Crear Proyecto";
      this.project = new Project('' ,'','','',2020,'','');
      //this.save_project = "";
      //this.status = '';
      this.filesToUpload = [];
      this.url = Global.url;


  }

  ngOnInit(): void {
  }

  onSubmit(form:any)
  {
    //Guardar datos
    
    this._projectService.saveProject(this.project).subscribe(
        response => 
        {
          if(response.project)
          {
            // Subir la imagen
            if(this.filesToUpload.length >=1)
            {
              this._uploadService.makeFileRequest(Global.url+'uploadImage/'+response.project._id, [], this.filesToUpload, 'image')
            .then((result:any) =>
            {
              this.save_project = result.project;
              this.status = 'success';
              form.reset();
              console.log(this.save_project);
              console.log(this.project);
              alert("Se ha salvado el proyecto con id: " + this.save_project._id + " correctamente");
              
              
            });
            }
            else
            {
              this.save_project = response.project;
              this.status = 'success';
              form.reset();
              console.log(this.save_project);
            }
          }
          else
          {
            this.status = 'failed';
          }
          console.log(this.save_project);
          alert("Se ha salvado el proyecto con id: " + this.save_project._id + " correctamente");
        },

        error =>
        {
          console.log(<any>error);
        }
      );
    
  }

  fileChangeEvent(fileInput:any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }
};
    
          
  

      



