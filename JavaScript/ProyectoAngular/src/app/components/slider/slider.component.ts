import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

declare var jQuery:any; // hay que declarar estas dos variables para que funcione jQuery
                        // en Angular.
declare var $:any;

@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input() anchura: number;
  @Output () getAutor = new EventEmitter();

  public autor: any;

  constructor() {

    this.anchura = 400;
    this.autor = {
      nombre: "Enrique Revuelta",
      website: "enriquerevueltaportfolio.es",
      instagram: "@gobuub82"
    };

   }

  ngOnInit(): void {


    ($('.slider') as any).bxSlider({
      mode: 'fade',
      captions: true,
      slideWidth: this.anchura,

    });
  }

  starEvent(event:any)
  {
   
    this.getAutor.emit(this.autor);
  }
}
