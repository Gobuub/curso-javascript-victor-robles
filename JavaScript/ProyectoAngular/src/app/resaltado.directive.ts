import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(public el: ElementRef) 
  { 
   
  }

  ngOnInit()
  {
    var resaltar = this.el.nativeElement;
      resaltar.style.background = "yellow";
      resaltar.style.padding = "20px";
      resaltar.style.marginTop = "15px";
      resaltar.style.color = "purple";
      resaltar.innerText = resaltar.innerText.toUpperCase();
  }
}
