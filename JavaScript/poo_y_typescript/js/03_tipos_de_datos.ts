// tipo de dato customizado con la palabra type, creamos variable y asignamos los tipos
// de datos que puede tener ese tipo de dato que usaremos posteriormente

type alfanumerico = string | number;



// string

let cadena: alfanumerico = "Hola, TS";

// usando la barra | podemos asignar varios tipos de datos a una misma variable

cadena = 12;

// number

let number: number = 12;

// boolean

let verdadero_falso: boolean = true;

// any

let cualquiera: any = "Hola";

cualquiera = 77;

// arrays

var lenguajes: Array<string> = ["PHP", "C#", "JS", "TypeScript", "CSS", "HTML"];


let years: number[] =[12, 243, 432, 543];

// let vs var, 

var numero1 = 10;
var numero2 = 12;

if (numero1 == 10){
	let numero1 = 44;
	var numero2 = 55;

	console.log(numero1 + " valor cambiado con 'let' ","y este " , numero2 + " tiene el valor cambiado con 'var'");

	// este mostrará en consola 44 y 55 porque let actua a nivel local o de bloque y
	// var actua a nivel global
}

// este mensaje mostrará 12 y 55 porque cambiamos el valor a nivel global dentro del "if"

console.log("'" + numero1 +"'" + " aquí vuelve a tener su valor original ","y", "'" +numero2+"'" + " aquí tiene el valor actualizado anteriormente");



console.log(cadena, number, verdadero_falso, cualquiera, lenguajes, years);

