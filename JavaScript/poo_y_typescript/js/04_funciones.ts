
function getNumero(numero = 12):string /* podemos asignar el tipo de dato que queremos que devuelva, y si
no lo hacemos nos devolverá el error al compilar, esto sirve para controlar el tipo de datos que entran y
salen de los métodos que desarrollemos, y de esta forma evitar futuros bugs en nuestros códigos*/
{
	return "El numero es: "+numero;
}

console.log(getNumero(45));