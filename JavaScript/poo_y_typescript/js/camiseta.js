// Interface
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Decorador
function estampar(logo) {
    return function (target) {
        target.prototype.estampacion = function () {
            console.log("Estampada con el logo de: " + logo);
        };
    };
}
//Clases
// Clase (molde del objeto)
var Camiseta = /** @class */ (function () {
    // Métodos (funciones o acciones del objeto)
    // Constructores se usan para darles valor a las propiedades de una clase
    function Camiseta(color, modelo, marca, talla, precio) {
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }
    Camiseta.prototype.setColor = function (color) {
        this.color = color;
    };
    Camiseta.prototype.getColor = function () {
        return this.color;
    };
    Camiseta = __decorate([
        estampar("Adidas Street Wear")
    ], Camiseta);
    return Camiseta;
}());
// Clase hija
var Sudadera = /** @class */ (function (_super) {
    __extends(Sudadera, _super);
    function Sudadera() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sudadera.prototype.setCapucha = function (capucha) {
        this.capucha = capucha;
    };
    Sudadera.prototype.getCapucha = function () {
        return this.capucha;
    };
    return Sudadera;
}(Camiseta));
var camiseta = new Camiseta("Rojo", "Manga Larga", "Adidas", "S", 15);
camiseta.estampacion();
console.log(camiseta);
var sudadera_adidas = new Sudadera("Negro", "Manga Larga", "Adidas", "S", 30);
sudadera_adidas.setCapucha(true);
sudadera_adidas.setColor("Negro y Gris");
sudadera_adidas.estampacion();
console.log(sudadera_adidas);
//var camiseta = new Camiseta("Rojo", "Manga Larga", "Adidas", "S", 15);
//camiseta.setColor("Rojo");
//var maillot = new Camiseta("Amarillo Fluor", "Manga Corta", "Gobik", "M", 45);
//maillot.setColor("Amarillo Fluor");
/* esto solo sirve si las propiedades de la clase son públicas
var camiseta = new Camiseta();

camiseta.color = "Rojo";
camiseta.modelo = "Manga Larga";
camiseta.marca = "Adidas";
camiseta.talla = "S"
camiseta.precio = 20;

var maillot = new Camiseta();

maillot.color = "Amarillo Fluor";
maillot.modelo = "Manga Corta";
maillot.marca = "Gobik";
maillot.talla = "M"
maillot.precio = 45;

*/
//console.log(camiseta, maillot);
