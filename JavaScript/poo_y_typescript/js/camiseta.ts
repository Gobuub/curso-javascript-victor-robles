// Interface

interface CamisetaBase{
	setColor(color);
	getColor();

}

// Decorador

function estampar(logo: string){
	return function(target: Function){
		target.prototype.estampacion = function(): void{
			console.log ("Estampada con el logo de: " + logo);
		}
	}
}

//Clases

// Clase (molde del objeto)
@estampar("Adidas Street Wear")
class Camiseta implements CamisetaBase
// las clases siempre empiezan en mayuscula y a ser posible con el mismo nombre que el archivo
{
	// Propiedades (caracteristicas del objeto)
	private color: string;
	private modelo: string;
	private marca: string;
	private talla: string;
	private precio: number;

	// Métodos (funciones o acciones del objeto)

	// Constructores se usan para darles valor a las propiedades de una clase
	constructor (color, modelo,marca, talla, precio){
		this.color = color;
		this.modelo = modelo;
		this.marca = marca;
		this.talla = talla;
		this.precio = precio;

	}

	public setColor(color) {
		this.color = color;
	}

	public getColor(){
		return this.color
	}


}

// Clase hija

class Sudadera extends Camiseta
{
	public capucha: boolean;

	setCapucha(capucha: boolean){
		this.capucha = capucha;
	}

	getCapucha():boolean{
		return this.capucha;
	}
}

var camiseta = new Camiseta ("Rojo", "Manga Larga", "Adidas", "S", 15);
camiseta.estampacion();

console.log(camiseta);

var sudadera_adidas = new Sudadera("Negro", "Manga Larga", "Adidas", "S", 30);
sudadera_adidas.setCapucha(true);
sudadera_adidas.setColor("Negro y Gris");
sudadera_adidas.estampacion();
console.log(sudadera_adidas);

//var camiseta = new Camiseta("Rojo", "Manga Larga", "Adidas", "S", 15);
//camiseta.setColor("Rojo");

//var maillot = new Camiseta("Amarillo Fluor", "Manga Corta", "Gobik", "M", 45);
//maillot.setColor("Amarillo Fluor");


/* esto solo sirve si las propiedades de la clase son públicas
var camiseta = new Camiseta();

camiseta.color = "Rojo";
camiseta.modelo = "Manga Larga";
camiseta.marca = "Adidas";
camiseta.talla = "S"
camiseta.precio = 20;

var maillot = new Camiseta();

maillot.color = "Amarillo Fluor";
maillot.modelo = "Manga Corta";
maillot.marca = "Gobik";
maillot.talla = "M"
maillot.precio = 45;

*/

//console.log(camiseta, maillot);




