var socket = io.connect('http://192.168.1.13:6677',{'forceNew': true});

socket.on('messages', function(data)
{
	console.log(data);
	render(data); // llamamos a la función para que nos imprima el contenido en el html
});

function render(data)
{
	var html = data.map(function(message, index)
	{
		return(`

			<div class="message">
				<strong>${message.nickname}</strong> dice:
				<p>${message.text}</p>
			</div>
			`);
		//Con las comillas invertidas hacemos que escriba las líneas que necesitemos
	}).join(' ');

	var div_msgs = document.getElementById('messages');
	div_msgs.innerHTML = html;
	div_msgs.scrollTop = div_msgs.scrollHeight;
	// con este método hacemos que nos imprima en el html en la zona que nosotros le marcamos, la variable de html
}

function addMessage(e)
{
	var message = {
		nickname: document.getElementById('nickname').value, // esto nos recoge la info del campo nickname
		text: document.getElementById('text').value // esto del campo text
	};

	socket.emit('add-message', message);
	// con esto recogemos la info del usuario y lo mandamos al servidor

	document.getElementById('nickname').style.display = 'none'; 
	// con esto ocultamos el campo nickname para que no pueda cambiarlo durante la conversación
	

	console.log(message);
	return false;
}