var express = require('express'); 
// cargamos express
var app = express(); 
// llamamos a la función express
var server = require ('http').Server(app); 
/* creamos una variable para el servidor y cargamos
la libreria http y le pasamos nuestra app como variable */
var io = require('socket.io')(server, {
	cors: {// permite el acceso de origenes mixtos (CORS)
		origin: '*'
	}
});
// cargamos la libreria y le pasamos el servidor

app.use(express.static('client'));

app.get('/hola-mundo', function(req, res){
	res.status(200).send('Hola mundo desde una ruta');

});
//probamos que la ruta funciona dentro del servidor

var messages = [{
	id: 1,
	text: "Bienvenido al chat privado de socket.io y NodeJS de Enrique Revuelta",
	nickname: "Bot - Gobuub"
}]

io.on('connection', function(socket)
{
	console.log("El cliente con IP: " + socket.handshake.address + " se ha conectado.");

	socket.emit('messages', messages);

	socket.on('add-message', function(data){
		messages.push(data); // añadimos el elemento al array

		io.sockets.emit('messages', messages);
	});
});
// este método detecta que un cliente se ha conectado a nuestro socket y nos lanza este mensaje



server.listen(6677, function()
	{
		console.log("El servidor está funcionando en http://localhost:6677");
		// probamos a ver si funciona nuestro servidor
	});